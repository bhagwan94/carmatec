# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200408094254) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cart_products", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "product_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_cart_products_on_customer_id", using: :btree
    t.index ["product_id"], name: "index_cart_products_on_product_id", using: :btree
  end

  create_table "colors", force: :cascade do |t|
    t.string   "name"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_colors_on_product_id", using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "email",      null: false
    t.string   "mobile",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "url"
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id", using: :btree
  end

  create_table "merchants", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "address",    null: false
    t.string   "email",      null: false
    t.string   "website",    null: false
    t.string   "about",      null: false
    t.string   "phone",      null: false
    t.string   "mobile",     null: false
    t.string   "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_attributes", force: :cascade do |t|
    t.string   "product_unit"
    t.float    "price"
    t.integer  "product_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["product_id"], name: "index_product_attributes_on_product_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "pid"
    t.string   "name"
    t.float    "price"
    t.string   "description"
    t.integer  "merchant_id"
    t.integer  "brand_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["brand_id"], name: "index_products_on_brand_id", using: :btree
    t.index ["merchant_id"], name: "index_products_on_merchant_id", using: :btree
  end

  create_table "variants", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_variants_on_product_id", using: :btree
  end

  create_table "wishlists", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "product_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_wishlists_on_customer_id", using: :btree
    t.index ["product_id"], name: "index_wishlists_on_product_id", using: :btree
  end

  add_foreign_key "cart_products", "customers"
  add_foreign_key "cart_products", "products"
  add_foreign_key "product_attributes", "products"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "merchants"
  add_foreign_key "variants", "products"
  add_foreign_key "wishlists", "customers"
  add_foreign_key "wishlists", "products"
end
