class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.string :name,  null: false
      t.string :email,  null: false,  unique: true
      t.string :mobile,  null: false

      t.timestamps
    end
  end
end
