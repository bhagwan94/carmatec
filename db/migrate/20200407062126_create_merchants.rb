class CreateMerchants < ActiveRecord::Migration[5.0]
  def change
    create_table :merchants do |t|
      t.string :name, null: false
      t.string :address, null: false
      t.string :email, null: false, unique: true
      t.string :website, null: false
      t.string :about, null: false
      t.string :phone, null: false
      t.string :mobile, null: false
      t.string :logo

      t.timestamps
    end
  end
end
