class CreateProductAttributes < ActiveRecord::Migration[5.0]
  def change
    create_table :product_attributes do |t|
      t.string :product_unit
      t.float :price
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
