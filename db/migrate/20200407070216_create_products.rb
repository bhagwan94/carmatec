class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :pid, unique: true
      t.string :name
      t.float :price
      t.string :description
      t.references :merchant, foreign_key: true
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end
