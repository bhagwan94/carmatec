# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
customer = Customer.create(name: "bhagwan", email: "bhagwan@gmail.com", mobile: "7899437413")
Brand.create([{name: "Samsung"}, {name: "Nike"}, {name: "Apple"}, {name: "Indian"}, {name: "Tata"}, {name: "U.S. Polo Assn."}])
Merchant.create([
  {logo: "https://dynamic.brandcrowd.com/asset/logo/523ee409-81f7-4d7a-900d-81e1e2473a2b/logo?v=4", name: "Merchant1", email: "merchant1@gmail.com", phone: "7899437413", address: "pai layout, banglore-560016", website: "merchant1.website.com", mobile: "7899437413", about: "about merchant1"},
  {logo: "https://image.shutterstock.com/image-vector/logo-d-letter-isolated-on-260nw-357580253.jpg", name: "Merchant2", email: "merchant2@gmail.com", phone: "7899437413", address: "pai layout, banglore-560016", website: "merchant2.website.com", mobile: "7899437413", about: "about merchant2"}])
