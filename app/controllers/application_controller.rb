class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_customer

  def set_customer
    @customer ||= Customer.first
  end

end
