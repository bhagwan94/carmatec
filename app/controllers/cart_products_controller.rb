class CartProductsController < ApplicationController
  before_action :set_cart_product, only: [:show, :edit, :update, :destroy]
  before_action :set_post_params, only: [:create]

  # GET /cart_products
  # GET /cart_products.json
  def index
    @cart_products = @customer.cart_products
  end

  # GET /cart_products/1
  # GET /cart_products/1.json
  def show
  end

  # GET /cart_products/new
  def new
    @cart_product = CartProduct.new
  end

  # GET /cart_products/1/edit
  def edit
  end

  def add_cart_product
    @cart_product = CartProduct.new(cart_product_params)
  end

  # POST /cart_products
  # POST /cart_products.json
  def create
    @cart_product = CartProduct.new(set_post_params)

    product = @cart_product.product

    respond_to do |format|
      if @cart_product.save
        format.html { redirect_to merchant_product_path(product.merchant, product), notice: 'Cart product was successfully created.' }
        format.json { render :show, status: :created, location: merchant_product_path(product.merchant, product) }
      else
        format.html { render :new }
        format.json { render json: @cart_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cart_products/1
  # PATCH/PUT /cart_products/1.json
  def update
    respond_to do |format|
      if @cart_product.update(cart_product_params)
        format.html { redirect_to @cart_product, notice: 'Cart product was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart_product }
      else
        format.html { render :edit }
        format.json { render json: @cart_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cart_products/1
  # DELETE /cart_products/1.json
  def destroy
    @cart_product.destroy
    respond_to do |format|
      format.html { redirect_to cart_products_url, notice: 'Cart product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart_product
      @cart_product = CartProduct.find(params[:id])
    end

    def set_post_params
      params.permit(:customer_id, :product_id)
    end

    # Only allow a list of trusted parameters through.
    def cart_product_params
      params.require(:cart_product).permit(:customer_id, :product_id)
    end
end
