json.extract! product, :id, :name, :price_per_unit, :unit, :description, :merchant_id, :product_type_id, :created_at, :updated_at
json.url product_url(product, format: :json)
