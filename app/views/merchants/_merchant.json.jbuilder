json.extract! merchant, :id, :name, :address, :email, :website, :about, :phone, :mobile, :created_at, :updated_at
json.url merchant_url(merchant, format: :json)
