class Product < ApplicationRecord
  belongs_to :merchant
  belongs_to :brand, optional: true

  has_many :product_attributes
  has_many :variants
  has_many :images, as: :imageable
  has_many :colors

end
