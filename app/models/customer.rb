class Customer < ApplicationRecord
  has_many :cart_products
  has_many :wishlists
end
