require File.expand_path('../../config/environment', __FILE__)

require_relative 'data.rb'


PRODUCTS.each do |data|
  ActiveRecord::Base.transaction do
    if data
      product = Product.new(name: data[:name], description: data[:description], pid: data[:pid])
      p "producttttttttttttt === #{data[:pid]} "
      if data[:price]
        product.price = data[:price]
      end

      if data[:brand]
        p "add product branddddddddddd === #{data[:brand]} "
        brand = Brand.find_by(name: data[:brand])
        product.brand = brand
      end

      if data[:merchant]
        p "add product Merchant === #{data[:merchant]} "
        merchant = Merchant.find_by(name: data[:merchant])
        product.merchant = merchant
      end

      product.save!

      if data[:size]
        p "add product Merchant === #{data[:size]} "
        variant = Variant.new(name: data[:size], price: data[:price])
        variant.product = product
        variant.save

        if data[:color]
          p "add product Merchant === #{data[:size]} "
          color = Color.new(name: data[:color])
          color.product = product
          color.save
        end

      end

      if data[:product_unit]
        p "add product Merchant === #{data[:product_unit]} "
        attribute = ProductAttribute.new(product_unit: data[:product_unit], price: data[:price])
        attribute.product = product
        attribute.save
      end

    end
  end

end


IMAGES.each do |data|
  ActiveRecord::Base.transaction do
    if data[:images]
      product = Product.find_by(pid: data[:pid])

      data[:images].each do |url|
        #image = Image.create!(url: url, imageable_type: product.class.name, imageable_id: product.id)
        image = Image.create!(url: url, imageable: product)
      end
    end
  end

end
