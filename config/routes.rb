Rails.application.routes.draw do

  #resources :wishlists
  #resources :carts
  resources :products
  resources :merchants do
    resources :products, only: [:index, :show]
  end
  #resources :customers
  resources :customers do
    resources :products do
      resources :cart_products, only: [:create]
        resources :wishlists, only: [:create]
    end
    resources :cart_products, only: [:index]
    resources :wishlists, only: [:index]
  end
  resources :cart_products
  resources :wishlists
  root "merchants#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
