# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

Note: 1) Since there is no login functionality, first customer is logged in user.

* Ruby version 2.6.3

* Rails version 5.0.7.

* System Config - Linux Mint 19.1 Tessa 64-bit

* Steps to run
 1) bundle install
 2) rake db:create
 3) rake db:migrate
 4) rake db:seed
 5) ruby scripts/create_products.rb #script to create products
 6) rails s

 * Gems used
  - pg
  - ransack
